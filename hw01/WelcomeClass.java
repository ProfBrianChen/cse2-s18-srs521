// Srimitha Srinivasan 
// 1/29/18
// CSE002

public class WelcomeClass{ //start of public class 
  public static void main(String[] args){  //start of main method 
    System.out.println("  -----------");  //prints the line within the quotation marks  
    System.out.println("  | WELCOME |");  //prints the lines within the quotation marks 
    System.out.println("  -----------");  //prints the lines within the quotation marks 
    System.out.println("  ^  ^  ^  ^  ^  ^");  //prints the lines within the quotation marks 
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");  //prints the lines within the quotation marks 
    System.out.println("<-S--R--S--5--2--1->");  //prints the lines within the quotation marks 
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");  //prints the lines within the quotation marks 
    System.out.println("  v  v  v  v  v  v");  //prints the lines within the quotation marks 
    System.out.println(" My name is Srimitha Srinivasan.  I have a little sister in 8th grade.  I love to read and write.  ");
    
  }   //end main method 
    }  //end public class 
  


