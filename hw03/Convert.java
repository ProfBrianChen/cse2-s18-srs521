//Srimitha Srinivasan 
//2/9/18
//CSE002

//Program #1
//Write a program that asks the user for doubes that represent the number of acres of land affected by the hurricane 
//And how mnay inches of rain were dropped on average.  
//Convert the quanity of rain into cubic miles 

import java.util.Scanner; //import scanner 

public class Convert{ //start public class 
  public static void main(String[] args){  //start main method 
    Scanner myScanner = new Scanner (System.in);  //declare the instance of the scanner object, and call the scanner 
    //this will tell the scanner that you are constructing an instance in which it will take input from 
    
    System.out.println("Please enter the affected area in acres: "); //prompts the user to enter the affected area in acres 
    double area=myScanner.nextDouble();//stores the input from the user into the variable area 
    
    System.out.println("Please enter the inches of rain in the affected area: "); //prompts the user to enter the amount of rainfall in inches 
    double rainfall=myScanner.nextDouble();//stores the input give by the user into the variable rainfall
    
    double Liters; //declaring variable 
    double Gallons; //declaring variable 
    double cubicMiles; //declaring variable 
    double hurricanePrecipitation; //declaring variable 
    
    hurricanePrecipitation=area*rainfall; //equation for calculating hurricane precipitation in inches acres 
    Liters=hurricanePrecipitation*102790.15461; //converting inches acres too liters 
    Gallons=Liters*0.264172; //converting liters to gallons 
    cubicMiles=Gallons*(9.08169e-13);  //converting gallons to cubic miles, the final answer 
    
    System.out.println("The total precipitation in the affected area is: " + cubicMiles + "cubic miles.");  //final print statement statig the total precipitation in cubic miles 
    
    
    
  }//end main method 
  
}//end public class 