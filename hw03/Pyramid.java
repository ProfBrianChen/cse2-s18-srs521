//Srimitha Srinivasan 
//2/9/18 
//CSE002 

//Write a program that prompts the user for the dimensions of a pyramid
//Returns the volume inside the pyramid 

import java.util.Scanner; //import scanner 

public class Pyramid{  //start public class 
  public static void main(String[] args){  //start main method 
    Scanner myScanner = new Scanner(System.in);  //declare the isntance of a scanner method, and call the scanner method
    
    System.out.println("Enter the length of the square side of the pyramid: "); //prompts the user to enter the length of the square side 
    double squareSide=myScanner.nextDouble(); //take the input given from the user and store it in variable squareSide
    
    System.out.println("Enter the height of the pyramid: ");  //prompts the user to enter the height of the pyramid 
    double height=myScanner.nextDouble(); //take the input given by the user and store it into the height variable 
    
    double volume; //declares volume as a double 
    
    volume=(squareSide*squareSide*height)/3; //formula for volume of a pyramid 
    
    System.out.println("The volume of the pyramid with height is: " + volume); //states the volume of the pyramid as a final print statement 
    
    
  } //end main method 
  
} //end public class 