Grade:     95/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
Yes
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
The print statements print out extra decimal places.
C) How can any runtime errors be resolved?
Multiply by 100, convert to int, divide result by 100.
D) What topics should the student study in order to avoid the errors they made in this homework?
Reread the hint in the homework document. 
E) Other comments:
In the future, you can put your declarations and initializations on the same line to make the code shorter.