//Srimitha Srinivasan
//2/3/18
//CSE002 

public class Arithmetic{ //Start of public class 
  
  public static void main(String[] args){  //Start of main method 
    
    int numPants=3; //assigning value to variable declared variable, numner of pants  
    double pantsPrice=34.98; //assigning value to variable declared variable, cost of pants  
    
    int numShirts=2;  //assigning value to variable declared variable, number of shirts 
    double shirtPrice=24.99; //assigning value to variable declared variable, cost of shirts  
    
    int numBelts=1; //assigning value to variable declared variable, number of belts  
    double beltCost=33.99; //assigning value to variable declared variable, cost of belts  
    
    double paSalesTax=0.06; //assigning value to variable declared variable, decimal value of tax 
    
    double totalCostOfPants; //declaring total cost of pants
    double totalCostOfShirts; //declaring total cost of shirts
    double totalCostOfBelts;  //declaring total cost of belts 
    
    double totalCostOfPantsWTax; //declaring total cost of pants with sales tax
    double totalCostOfShirtsWTax; //declaring total cost of shirts with sales tax 
    double totalCostOfBeltsWTax;  //declaring total cost of belts with sales tax 
    
    double totalCost; //total cost of pants, shirts, belts 
    double totalSalesTax; //total sales tax
    double totalShoppingCost; //total cost of clothing and sales tax together 
    
    double salesTaxPants; //declaring pants sales tax 
    double salesTaxShirts; //declaring shirts sales tax 
    double salesTaxBelts; //declaring belts sales tax
    
    totalCostOfPants=numPants*pantsPrice; //total cost of pants 
    totalCostOfShirts=numShirts*shirtPrice; //total cost of shirts 
    totalCostOfBelts=numBelts*beltCost;  //total cost of belts 
    
    salesTaxPants=totalCostOfPants*paSalesTax;//computing sales tax for pants 
    salesTaxShirts=totalCostOfShirts*paSalesTax; //computing sales tax for shirts 
    salesTaxBelts=totalCostOfBelts*paSalesTax; //computing salles tax for belts 
    
    totalCostOfPantsWTax=(salesTaxPants)+totalCostOfPants; //finds the tax for the amount of pants bought and adds it to the totla cost of just the pants 
    totalCostOfShirtsWTax=(salesTaxShirts)+totalCostOfShirts; //finds the tax for the amount of pants bought and adds it to the total cost of just the shirts
    totalCostOfBeltsWTax=(salesTaxBelts)+totalCostOfBelts; //finds the tax for the amount of pants bought and adds it to the total cost of just the belts
    
    totalCost=totalCostOfPants+totalCostOfShirts+totalCostOfBelts;  //computing total cost without tax
    totalSalesTax=salesTaxPants+salesTaxShirts+salesTaxBelts; //compuitng total sales tax 
    totalShoppingCost=totalSalesTax+totalCost;  //computing total cost with sales tax

    System.out.println("The total cost cost of pants is " + totalCostOfPants);  //diaplay total cost of pants 
    System.out.println("The total cost of shirts is " + totalCostOfShirts); //display total cost of shirts 
    System.out.println("The total cost of belts is " + totalCostOfBelts);  //display total cost of belts 
    
    System.out.println("The sales tax charged on belts is " + salesTaxBelts);  //display total sales tax on belts
    System.out.println("The sales tax charged on shirts is " + salesTaxShirts);  //display total sales tax on shirts 
    System.out.println("The sales tax charged on pants is " + salesTaxPants);  //display total sales tax on pants 
    
    System.out.println("The total cost of clothing before tax is " + totalCost);  //display total cost before tax 
    System.out.println("The total sales tax is " + totalSalesTax);  //display total cost of sales tax 
    System.out.println("The total shopping cost with the sales tax is " + totalShoppingCost); //display total cost with tax 
  } //end of main method 
  
} //end of public class 