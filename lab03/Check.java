//Srimitha Srinivasan 
//2/9/18
//CSE002 

//The user has gone out to dinner with friends. After they receive the bill, they decide to split the check evenly. 
//Write a program that uses the Scanner class to obtain from the user the original cost of the check
//the percentage tip they wish to pay
//the number of ways the check will be split. 
//Then determine how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner;// In order to sue the scanner class, you must first import it 

public class Check{ //Start public class 
  public static void main(String[] args){ //start main method 
  
  Scanner myScanner = new Scanner (System.in); //Declare an instance of the scanner object, and call the scanner constructor 
  //This will tell scanner that you are creating an isntance that will take input from STDIN
    
  System.out.println("Enter the original cost of the check in the form XX.XX: "); //Prompts the user for the original cost of the check 
    double checkCost = myScanner.nextDouble(); //This accepts the input that the user entered, as a double type named checkCost

  System.out.println("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //Prompts the user for the percentage tip
    double tipPercent = myScanner.nextDouble();  //This accepts the input that the user entered, as a double type named tipPercent
    
    tipPercent/=100;  //converts the percentage into a decimal by dividing by a 100
    
  System.out.println("Enter the number of people who went out to dinner: ");  //Prompts the user to enter the number of people who went out to dinner 
    int numPeople = myScanner.nextInt();  ////This accepts the input that the user entered, as a double type named numPeople
    
  double totalCost; //declaring variable 
  double costPerPerson; //declaring variable 
  int dollars; //declaring variable 
  int dimes; //declaring variable 
  int pennies; //declaring variable 
    
  totalCost=checkCost*(1+tipPercent); //calculating total cost of meal
  costPerPerson=totalCost/numPeople;  //divides the cost evenly for the amount of people there 
    
  dollars=(int) costPerPerson%10;   //explicitly casts double into an int, mod operator returns the remainder after the division  
  dimes=(int)(costPerPerson);       //explicitly casts double into an int
  pennies=(int)(costPerPerson*100)%10;      //explicitly casts double into an int, mod operator returns the remainder after the division  
  
  System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);   //Prints the final statement of how much each person in the group owes 
    
    
    
    
  
  
  }//end main methood 
}//end public class 