//Srimitha Srinivasan 
//CSE002 
//2/2/18

public class Cyclometer{ //Start public class 
  public static void main(String[] args){  //Start main method 

    int trip1Time=480; //Declaring variable trip1Time and assigning value to variable 
    int trip1Rotations=1561; //Declaring variable trip1Rotoations and assigning value to variable 
    int trip2Time=3220; //Declaring variable trip2Time and assigning a value to variable 
    int trip2Rotations=9037; //Declaring variable trip2Rotations and assigning a value to variable 
    
    double wheelDiameter=27.0;  //declaring diameter of a wheel and assigning value 
    double PI=3.14159;  //declaring pi as a double and assigning value to variable 
    int feetPerMiles=5280; //declaring feetPerMiles as an int and assigning value to variable  
    int inchesPerFoot=12;  //declaring inchesPerFoot as an int and assigning value to variable 
    int secondsPerMinute=60; //declaring secondsPerMinute as a variable and assigning value to variaable 
  
    double distanceTrip1; //declaring distanceTrip1 as a variable  
    double distanceTrip2; //declaring distanceTrip2 as a variable 
    double totalDistance; //declaring totalDistance as a variable 
    
    System.out.println("Trip 1 took " + (trip1Time/secondsPerMinute) + " minutes and had " + trip1Rotations ); //Prints out the statement of how long trip 1 took and how many rotations 
    System.out.println("Trip 2 took " + (trip2Time/secondsPerMinute) + " minutes and had " + trip2Rotations ); //Prints out the statement of how long trip 2 took and how many rotations
    
    distanceTrip1=trip1Rotations*wheelDiameter*PI; //Calculating distance of trip 1 in inches  
    distanceTrip1=inchesPerFoot*feetPerMiles;  //converts the distance calculated into miles 
    distanceTrip2=trip2Rotations*wheelDiameter*PI/inchesPerFoot/feetPerMiles; //Calculates the distance of trip 2 in inches and then converts to miles

    totalDistance=distanceTrip1+distanceTrip2; //adds total distance of trip 1 and trip 2
    
  System.out.println("Trip 1 was "+distanceTrip1+" miles");  //Prints sthe distance of trip 1 
	System.out.println("Trip 2 was "+distanceTrip2+" miles");  //prints the distance of trip 2 
	System.out.println("The total distance was " + totalDistance + " miles");  //Prints the total distance of trip and 2 

    
    

    
    
    
    
    
    
    
  } //end main method 
  
}  //end public class 
