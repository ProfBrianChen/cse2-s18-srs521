//CSE 2 Hello World 
//Srimitha Srinivasan 
//1/26/18
///

public class HelloWorld { //Beginning of class 
  
  public static void main(String[] args) { //Beginning of main method 
   
    System.out.println("Hello, World"); //Print statement allows you to print "Hello World" in the terminal window
  } //End of main method 
}  //End of class
