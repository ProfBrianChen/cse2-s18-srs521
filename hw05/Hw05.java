//Srimitha Srinivasan 
//3/6/18 
//CSE002 

//Write loops that asks the user to enter information relating to a course they are taking 
//Ask for the course number, department name, the number of times it meets in a week 
//the time the class starts, the instructor, and the number of students 
//Check to make sure user enters correct number 

import java.util.Scanner; //import the scanner to use 

public class Hw05{  //start public class 
  public static void main(String[] args ){  //start main method 
    Scanner myScanner = new Scanner(System.in); //declare the scanner named myScanner and give it the scanner instance in which it assigns it to the input that the user enters 
    
/////////////////////////Course Number///////////////////////////////////////////////////////////////////////////////////////    
    
    System.out.println("Enter your class's number.  "); //prompt the user to enter the course number 
    int classNumber; //declaring the course number as an integer 
    
    if(myScanner.hasNextInt()){  //checks the conditional using an if statement if the user's input is a integer 
      classNumber=myScanner.nextInt(); //assign the variable courseNumber to the integer entered by the user  
    } //end the if statement 
      
      else { //start the else statement if the user did not enter an integer 
        System.out.println("Invalid, enter your course number.  "); //prompts the user to reenter a number 
        myScanner.next(); //clears the old value and creates space for the new value 
      } //end the else statement 
      
////////////////////Department Name///////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    System.out.println("Enter the department name.  "); //prompts the user to enter the department name 
     String departmentName; //decalres the variable department name as a string 
      
     while(true){ //starts the while loop, with condition if true 
       if (myScanner.hasNextInt()){  //checks to see if the input that user entered is an integer insteadd of a string
         System.out.println("Invalid, enter a department name."  ); //prompts the user to enter a strong not an integer 
         myScanner.next(); //clears the line if the user entered an integer and makes space for a new entry or input 
       } //closes if statement 
       
       else{ //starts else statement if the user entered a string 
         departmentName = myScanner.next(); //departmentName variable is assigned to the input that is recieved from the user due to the use of a scanner 
         break; //break and stop the if statement 
       } //ends else statement 
     } //ends the while loop 
    
//////////////Number of Times You Meet For Class ///////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    System.out.println("Enter the number of times that you meet for class. "); //prompt the user to enter the number of times that you met for class 
    int classAttendance; //declaring the number of times you attend class 
    
    if(myScanner.hasNextInt()){  //checks the conditional using and if statement if the user's input is a integer 
      classAttendance=myScanner.nextInt(); //assign the variable courseNumber to the integer entered by the user  
      } //end the if statement 
      
      else{ //start the else statement if the user did not enter an integer 
        System.out.println("Invalid, enter the number of times that you went to class. "); //prompts the user to reenter a number 
        myScanner.next(); //clears the old value and creates space for the new value 
      } //end the else statement   
    
////////////Time that the Class Starts///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    System.out.println("Enter the time that the class starts without the colon.  "); //prompts the user to enter a class time 
    Float classTime; //declares class time as a float variable because 
    
    while(true){ //starts while loop, the condition that the body is true 
      if(myScanner.hasNextFloat()){ //checks to see if the input that the user entered is a float
        classTime=myScanner.nextFloat(); //class time that the variable is assigned to the input that is recieved from the user due to the user of a scanner 
        break; //break and stop the if statement 
      } //stops if statement 
      
      else{  //starts the else statement if the user did not enter a float 
        System.out.println("Invalid, enter the class time without the colon.  "); //prompts the user to enter a floaot
      } //ends the else statement 
    }//ends while loop 

////////////////Instructor's Name //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
     
    System.out.println("Your teacher's name.  "); //prompts the user to enter the teacher's name
     String teacherName; //decalres the variable teacher name as a string 
      
     while(true){ //starts the while loop, with condition if true 
       if (myScanner.hasNextInt()){  //checks to see if the input that user entered is an integer insteadd of a string
         System.out.println("Invalid, enter a teacher's name.  "  ); //prompts the user to enter a string not an integer 
         myScanner.next(); //clears the line if the user entered an integer and makes space for a new entry or input 
       } //closes if statement 
       
       else{ //starts else statement if the user entered a string 
         teacherName = myScanner.next(); //teacher's name variable is assigned to the input that is recieved from the user due to the use of a scanner 
         break; //break and stop the if statement 
       } //ends else statement 
     } //ends the while loop 

////////////////Number of Students r/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    System.out.println("Enter the number of students in the class.  ");
     int studentNumber; //declaring the number of student in the class 
    
    if(myScanner.hasNextInt()){  //checks the conditional using and if statement if the user's input is a integer 
      studentNumber=myScanner.nextInt(); //assign the variable studentNumber to the integer entered by the user  
    } //end the if statement 
      
      else{ //start the else statement if the user did not enter an integer 
        System.out.println("Invalid, enter the number of students in the class.  "); //prompts the user to reenter a number 
        myScanner.next(); //clears the old value and creates space for the new value 
      } //end the else statement   
    
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    }// end main method 
}// end public class 