//Srimitha Srinivasan 
//2/20/18
//CSE002

//Yahtzee involves rolling five dice and scoring the result of the roll.
//Perform a random roll of the dice and score the roll.  
//Compute the scores and and 
//Print out the following values: upper section initial total, upper section total including bonus, lower section total, and grand total.
//The grand total is the sum of the upper section total including bonus and the lower section total.

///////////////////SETTING UP THE GAME//////////////////////////////////////

import java.util.Random; //This imports the ramdom geberator 
import java.util.Scanner; //This imports the scanner to use 

public class Yahtzee{ //Start public class 
  public static void main(String []args){  //Start main method 
    Scanner myScanner = new Scanner(System.in);  //Declare scanner variable, this allows you to have user input 
    
//User Can roll dice randomly or enter a five digit number 
      System.out.println("To roll random, enter 1, to type in 5 digits, enter 5.  ");  //Prompt the user to either roll ramdom by entering 1 or type in 5 digits by entering 5 
    int choose = myScanner.nextInt(); //Takes either the digit 1 or digit 5 and stores it in the variable choose which is declared as an integer 
    
    int diceOne = (int)(((Math.random())+5)+1);//Declare diceOne as a variable and allow it to choose from the 6 sides of a die.  Upper bound 5 because we have 5 dice, and add one because we need to start at 1 not 0.   
    int diceTwo = (int)(((Math.random())+5)+1);//Declare diceTwo as a variable and allow it to choose from the 6 sides of a die.  Upper bound 5 because we have 5 dice, and add one because we need to start at 1 not 0.   
    int diceThree = (int)(((Math.random())+5)+1);//Declare diceThree as a variable and allow it to choose from the 6 sides of a die.  Upper bound 5 because we have 5 dice, and add one because we need to start at 1 not 0.   
    int diceFour = (int)(((Math.random())+5)+1);//Declare diceFour as a variable and allow it to choose from the 6 sides of a die.  Upper bound 5 because we have, and add one because we need to start at 1 not 0.   
    int diceFive = (int)(((Math.random()+5))+1);  //Declare diceFive as a variable and allow it to choose from the 6 sides of a die.  Upper bound 5 because we have, and add one because we need to start at 1 not 0.   
    
       if ( choose==5 ){ //Start if and elses statement 
         System.out.println("Please enter 5 digits."); //prompts user to enter 5 digits 
         int fiveDigits = myScanner.nextInt(); //Declares a new variable fiveDigits which stores the 5 digits that the user enteres  
         
         diceOne=fiveDigits%10; //declares die one 
         diceTwo=fiveDigits/10%10; //declares die two 
         diceThree=fiveDigits/100%10; //declares die three 
         diceFour=fiveDigits/1000%10;  //declares die 4  
         diceFive=fiveDigits/10000%10;  //declares die 5 
         
       } //end if and else statement 
    
       else if (choose == 1){ // if statement for if the person chooses 1, this is a random generator 
      
    } //end the if statement 
    
       else{
         System.out.println("Wrong input.  Please enter digit 1 or 5 to decide how to roll the dice"); //If the person did not enter a 5 or a 1, we print the statement invalid and ask the user to enter a proper digit 
       }
    
      System.out.println("Dice one equals " + diceOne); //prints out what dice one equals 
      System.out.println("Dice two equals " + diceTwo); //prints out what dice two equals  
      System.out.println("Dice three equals " + diceThree); //prints out what dice 3 equals 
      System.out.println("Dice four equals " + diceFour);  //prints out what dice 4 equals 
      System.out.println("Dice five equals " + diceFive);  //prints out what dice 5 equals 
    
////////////////////////////UPPER SECTION////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////    
    
    int aces = 0; //declres and initialize aces, this is how we keep track of the score  
    int twos = 0; //decalres and initialzies twos, this is how we keep track of the score 
    int threes = 0; //declares and initializes threes, this is how we keep track of the score 
    int fours = 0; //decares and initializes fours, this is how we keep track of the score 
    int fives = 0; //declares and initializes fives, this is how we keep track of the score 
    int sixes = 0; //declares and initializes sixes, this is how we keep track of the score 
    
    //Using switch statements we list each possibilitiy (the number of aces to the number of sixes) for each die 
    
    switch (diceOne){ //start switch statement for dice 1
      case 1: 
        aces = aces +1; 
        break; 
      case 2: 
        twos = twos +1; 
        break;
      case 3: 
        threes = threes +1; 
        break;
      case 4: 
        fours = fours +1; 
        break;  
      case 5: 
        fives = fives +1; 
        break;  
      case 6: 
        sixes = sixes +1; 
        break;  
    } //end switch statement for dice 1
    
     //Using switch statements we list each possibilitiy (the number of aces to the number of sixes) for each die 
   switch (diceTwo){ //start switch statement for dice 2
      case 1: 
        aces = aces +1; 
        break; 
      case 2: 
        twos = twos +1; 
        break;
      case 3: 
        threes = threes +1; 
        break;
      case 4: 
        fours = fours +1; 
        break;  
      case 5: 
        fives = fives +1; 
        break;  
      case 6: 
        sixes = sixes +1; 
        break;  
    } //end switch statement for dice 2
    
     //Using switch statements we list each possibilitiy (the number of aces to the number of sixes) for each die 
   
    switch (diceThree){ //start switch statement for dice three 
      case 1: 
        aces = aces +1; 
        break; 
      case 2: 
        twos = twos +1; 
        break;
      case 3: 
        threes = threes +1; 
        break;
      case 4: 
        fours = fours +1; 
        break;  
      case 5: 
        fives = fives +1; 
        break;  
      case 6: 
        sixes = sixes +1; 
        break;  
    } //end switch statement for dice three 
     
    //Using switch statements we list each possibilitiy (the number of aces to the number of sixes) for each die 
    
   switch (diceFour){ //start switch statement for dice 4
      case 1: 
        aces = aces +1; 
        break; 
      case 2: 
        twos = twos +1; 
        break;
      case 3: 
        threes = threes +1; 
        break;
      case 4: 
        fours = fours +1; 
        break;  
      case 5: 
        fives = fives +1; 
        break;  
      case 6: 
        sixes = sixes +1; 
        break;  
    } //end switch statement for dice 4 
    
     //Using switch statements we list each possibilitiy (the number of aces to the number of sixes) for each die 
    
    switch (diceFive){ //start switch statement for dice 5 
      case 1: 
        aces = aces +1; 
        break; 
      case 2: 
        twos = twos +1; 
        break;
      case 3: 
        threes = threes +1; 
        break;
      case 4: 
        fours = fours +1; 
        break;  
      case 5: 
        fives = fives +1; 
        break;  
      case 6: 
        sixes = sixes +1; 
        break;  
    } //end switch statement for dice 5 
       
    int countAces = aces + 1; //declaration of the variable countAces which counts the total aces, and assigns it a value  
    int countTwos = twos + 1; //declaration of the variable countTwos which counts the total Twos, and assigns it a value  
    int countThrees = threes + 1; //declaration of the variable countThrees, which counts the total threes amd assigms it a value  
    int countFours = fours + 1; //declaration of the variable countFours, which counts the total fours, and assigns it a value 
    int countFives = fives + 1; //declaration of the variable countFives, which counts the total fives, and assigns it a value 
    int countSixes = sixes +1; //declarations of the variable countSixes, which counts the total sixes, and assigns it a value  
    
 System.out.println(" ");
 System.out.println("UPPER SECTION"); //Shows score for upper 
 System.out.println(" ");    
 System.out.println ("The total count for aces " + countAces); //prints the score for aces 
 System.out.println ("The total count for twos " + countTwos);  //prints the score for twos
 System.out.println ("The total count for threes " + countThrees); //prints the score for threes 
 System.out.println ("The total count for fours " + countFours); //prints the score for fours 
 System.out.println ("The total count for fives  " + countFives); //prints the score for fives 
 System.out.println ("The total count for sixes " + countSixes); //prints the score for sixes 
    
   int upperScore = ((aces *1) + (twos *2) + (threes *3) + (fours *4) + (fives *5) + (sixes * 6)); //calculates the total upper score, arithmetic 
   System.out.println("The total upper score is: " + upperScore);  //rints the total upper score 
    
    int bonusScore = 35; //given in the program instructions 
    int upperSectionTotal = (upperScore + bonusScore); //calculates the person's total score 
    //THE BONUS SCORE WILL BE USED IF THE UPPER SECTION IS GREATER THAN 63 
    if (upperScore >= 63){  //if the upper section is greater than 63, a bonus of 35 is added to the score when computing the upper section total 
      System.out.println(" ");
      System.out.println("The upper section total is: " + upperSectionTotal);   //Prints out the upper section total   
      System.out.println(" ");
    
//////////////////////////LOWER SECTION///////////////////////////////////////////////////////////////////////////////    
////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////////////////////////////////////////      
      
      System.out.println(" ");
      System.out.println ("LOWER SECTION"); //LOWER SECTION BEGINS 
      System.out.println(" ");
 
    int acesTotal = aces; //declaring total aces as int and assigning it to value aces 
    int twosTotal = twos; //declaring total twos as int and assigning it to value twos 
    int threesTotal = threes;  //declaring total threes as int and assigning it to value threes 
    int foursTotal = fours;  //declaring total fours as int and assigning it to value fours 
    int fivesTotal = fives;  //declaring total fives as int and assigning it to value fives 
    int sixesTotal = sixes; //declaring total sixes as int and assigning it to value six 
    
    int threeOfAKind = 0; //declare int and initializing three of a kind 
    int fourOfAKind = 0; //declare int and initializing fout of a kinf 
    int Yahtzee = 0; //declare int and initializing Yahtzee
    int fullHouse = 0; ////declare int and initializing a full house 
    int smallStraight = 0; ////declare int and initializing small straight 
    int largeStraight = 0;//declare int and initializing large straight 
    int chance = 0; //declare int and initializing chance 
      
      //Using the switch stateements we run through the different possibilities of the cases depending on what we roll 
    
       switch (acesTotal) { 
          case(3): 
        threeOfAKind += 1 * 3;
      if (twosTotal == 2 || threesTotal == 2 || foursTotal == 2 || fivesTotal == 2 || sixesTotal == 2){
        fullHouse += 1;
      }
          break; 
          case(4): 
        fourOfAKind += 1 * 4;
           break; 
           case(5):
        Yahtzee += 1; 
           break; 
    }
    
       //Using the switch stateements we run through the different possibilities of the cases depending on what we roll 
      
    switch (twosTotal) { 
    case(3): 
        threeOfAKind += 2 * 3; 
        if (acesTotal == 2 || threesTotal == 2 || foursTotal == 2 || fivesTotal == 2 || sixesTotal == 2){
          fullHouse += 1; 
          break;
        }
       if (acesTotal > 0 && twosTotal > 0 && threesTotal > 0 && foursTotal > 0){ 
          smallStraight += 30; 
         
       
       } else if (twosTotal > 0 && threesTotal > 0 && foursTotal > 0 && fivesTotal > 0) { 
          smallStraight += 30; 
         
       } else if (threesTotal > 0 && foursTotal > 0 && fivesTotal > 0 && sixesTotal >0) {
          smallStraight += 30; 
       }
    // based on the number of each value you get the score the special case scores get added on 
   
        //use if statements w figure out which we assign the variable to 
       if (acesTotal > 0){
    if (twosTotal > 0) { 
    if (threesTotal > 0) {
    if (foursTotal > 0) { 
    if (fivesTotal > 0) {
      largeStraight += 1;
    }  
      }  
        }
          }  
            } 
              }
    
     //use if statements w figure out which we assign the variable to 
      
      if (twosTotal > 0){
    if (threesTotal  > 0) { 
    if (foursTotal > 0) {
    if (fivesTotal > 0) { 
    if (sixesTotal > 0) {
      largeStraight += 1; 
    }
      }
        }
          }
            }
      
    
    int totalDieValue = diceOne + diceTwo + diceThree + diceFour + diceFive; //computers the total dice value score 
    chance = totalDieValue *1; // chance is the total die value  
                              //assigns the chance variable to the total die value score 
        
    int lowerSectionTotal = threeOfAKind + fourOfAKind + (fullHouse + 25) + (smallStraight + 30) + (largeStraight + 30) 
      + (Yahtzee + 50) + chance;  // add the number of each different type of components and the scores that get added on for rolling one of these components
        
        int totalScore = upperSectionTotal + lowerSectionTotal; //prints totla score by adding the lower and upper section 
        
        System.out.println("Your dice values were : " + diceOne + diceTwo + diceThree + diceFour + diceFive); //prints out total dice value score  
        System.out.println("Upper Section Score is : " + upperSectionTotal); //prints out the total upper section score 
        System.out.println("Lower Section Score is : " + lowerSectionTotal); //prints out the total lower section score 
        System.out.println("Total Score is : " + totalScore); //prints out the totla score 
    
    
  }
  }//end of main method 
    }//end of public class
  

